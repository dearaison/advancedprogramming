package formatdate;

import java.util.GregorianCalendar;

public class FormatDate {

	public static void main(String[] args) {
		printDate("29/32/2016");
		printDate("29/2/2016");

	}

	static String getYear(String date) {
		String year = date.substring(date.lastIndexOf("/") + 1);
		return year;
	}

	static boolean checkYear(String date) {
		int tempYear = Integer.parseInt(getYear(date));
		if (tempYear < 0)
			return false;
		else
			return true;
	}

	static String getMonth(String date) {
		String Month = date.substring(date.indexOf("/") + 1, date.lastIndexOf("/"));
		return Month;
	}

	static String formatMonth(String date) {
		String srcMonth = date.substring(date.indexOf("/") + 1, date.lastIndexOf("/"));
		String month = "";
		switch (srcMonth) {
		case "1":
			month = "January";
			break;
		case "2":
			month = "February";
			break;
		case "3":
			month = "March";
			break;
		case "4":
			month = "April";
			break;
		case "5":
			month = "May";
			break;
		case "6":
			month = "June";
			break;
		case "7":
			month = "July";
			break;
		case "8":
			month = "August";
			break;
		case "9":
			month = "September";
			break;
		case "10":
			month = "October";
			break;
		case "11":
			month = "November";
			break;
		case "12":
			month = "December";
			break;
		default: {
			return month = "this month is not correct";
		}
		}
		return month;
	}

	static String getDay(String date) {
		String day = date.substring(0, date.indexOf("/"));
		return day;

	}

	private static GregorianCalendar calendar = new GregorianCalendar();

	static boolean checkDay(String date) {
		int tempDay = Integer.parseInt(getDay(date));
		int tempMonth = Integer.parseInt(getMonth(date));
		int tempYear = Integer.parseInt(getYear(date));
		if (tempDay < 1 || tempDay > 31) {
			System.out.println("The day of month must be between 1 and 31.");
			return false;
		}
		if (tempDay > 30 && (tempMonth != 1 || tempMonth != 3 || tempMonth != 5 || tempMonth != 7 || tempMonth != 8
				|| tempMonth != 10 || tempMonth != 12)) {
			System.out.println("This month isn't 1, 3, 5, 7, 8, 10, 12, so the day of month must be less than 30.");
			return false;
		}
		if (tempDay > 29 && tempMonth == 2 && calendar.isLeapYear(tempYear)) {
			System.out.println("The day of february mus be less than 30 in leap year.");
			return false;
		}
		if ((tempDay > 28 && tempMonth == 2 && !calendar.isLeapYear(tempYear))) {
			System.out.println("This year isn't leap year, so the day of february must be less than 29.");
			return false;
		}
		return true;

	}

	static void printDate(String date) {
		if (checkYear(date) == true && formatMonth(date) == "this month is not correct" && checkDay(date) == true)
			System.out.println(formatMonth(date));
		else if (checkYear(date) == true && checkDay(date) == true)
			System.out.println(getDay(date) + " " + formatMonth(date) + " " + getYear(date) + " " + "AC");
		else if (checkYear(date) == false && checkDay(date) == true)
			System.out.println(getDay(date) + " " + formatMonth(date) + " " + getYear(date) + " " + "BC");

	}
}
