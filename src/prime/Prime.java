package prime;

public class Prime {
	public static void main(String[] args) {
		Prime prime= new Prime();
		System.out.println(new Prime().isPrime(12));
		new Prime().searchPrime(7, 6);
		prime.annlyzeANumberToPrime(15918);
	}

	boolean isPrime(int x) {
		if (x == 2 || x == 3)
			return true;
		if (x < 2)
			return false;
		for (int i = 2; i < x - 1; i++) {
			if (x % i == 0)
				return false;
		}
		return true;
	}
	// void creatSomePrime(int a, int b) {
	// int count = 0;
	// while (count < b){
	// if (isPrime(a)){
	// System.out.println(a);
	// count++;
	// }
	// a++;
	// }
	// }

	void searchPrime(int a, int b) {
		for (int count = 0; count < b; a++) {
			if (isPrime(a) == true) {
				System.out.println(a);
				count++;
			}
		}
	}

	public void annlyzeANumberToPrime(int aNumber) {
		int cNumber = aNumber;
		firstLoop: while (cNumber > 1) {
			for (int swapNum = 2; swapNum < aNumber - 1; swapNum++) {
				if (isPrime(swapNum) && cNumber % swapNum == 0) {
					cNumber /= swapNum;
					System.out.print(swapNum + "\t");
					continue firstLoop;
				}
			}
		}
	}
}
