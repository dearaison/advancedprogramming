package computestringcontaintwooperator;

public class ComputeAString {
	/**
	 * Task: input is a string contain numbers and two types of arithmetic operator
	 * sign. Two types of Arithmetic Operator are addition (+) and multiplication
	 * (x). Could you compute that string as expression and get the result number?
	 */
	public static double compute(String input) {
		double res = 0;
		double current;
		String[] add = input.split("\\+");
		for (String e : add) {
			try {
				current = Double.parseDouble(e);
			} catch (NumberFormatException e2) {
				current = 1;
				String[] temp = e.split("x");
				for (String a : temp) {
					current *= Double.parseDouble(a);
				}
			}
			res += current;
		}
		return res;
	}

	public static void main(String[] args) {
		System.out.println(compute("1+2x3+2x1x1+2"));
	}
}
