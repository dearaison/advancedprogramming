package formatnumber;

public class FormatNumber {

	static String formatNumber1(int aNumber)/* the first way */ {
		String a = "" + aNumber;
		String b = "";
		for (int i = a.length() - 1, count = 0; i >= 0; i--) {
			if (count == 3) {
				b = "," + b;
				count = 0;
			}
			b = a.charAt(i) + b;
			count++;
		}
		return b;
	}

	static String formatNumber2(int aNumber)/* the second way */ {
		String res = "";
		int even, remain;
		even = aNumber / 1000;
		remain = aNumber % 1000;
		if (remain < 10)
			res = "00" + remain + "";
		else if (remain < 100)
			res = "0" + remain + "";
		else
			res = remain + "";

		while (even > 0) {
			remain = even % 1000;
			even = even / 1000;
			if (remain < 10 && even > 0)
				res = "00" + remain + "," + res;
			else if (remain < 100 && even > 0)
				res = "0" + remain + "," + res;
			else
				res = remain + "," + res;
		}
		return res;
	}

	public static void main(String[] args) {
		System.out.println(formatNumber2(10200000));
	}
}
