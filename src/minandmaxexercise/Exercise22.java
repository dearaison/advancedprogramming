package minandmaxexercise;

public class Exercise22 {

	static int isMax(int[] array) {
		int max = array[0];
		for (int i : array) {
			if (i > max)
				max = i;
		}
		return max;
	}

	static int isMin(int[] array) {
		int max = array[0];
		for (int i : array) {
			if (i < max)
				max = i;
		}
		return max;
	}

	static int getIndexOfTheFirstMaxNumber(int[] array) {
		int index = 0;
		int max = isMax(array);
		for (int i = 0; i < array.length; i++) {
			if (array[i] == max)
				return index = i;
		}
		return index;

	}

	static int getIndexOfTheFirstMinNumber(int[] array) {
		int index = 0;
		int min = isMin(array);
		for (int i = 0; i < array.length; i++) {
			if (array[i] == min)
				return index = i;
		}
		return index;

	}

	static int computeTotalOfMaxNumber(int[] array) {
		int total = 0;
		int max = isMax(array);
		int count = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[i] == max)
				count++;
		}
		total = max * count;

		return total;
	}

	static int computeTotalOfMinNumber(int[] array) {
		int total = 0;
		int min = isMin(array);
		int count = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[i] == min)
				count++;
		}
		total = min * count;

		return total;
	}

	public static int returnGreaterNumber(int a, int b) {
		return (((a + b) + (Math.abs(a - b))) / 2);
	}

	public static void main(String[] args) {
		int[] array = { 1, 5, 9, 8, 9, 1, 2, 9, 1 };
		System.out.println(isMax(array));
		System.out.println(isMin(array));
		System.out.println(getIndexOfTheFirstMaxNumber(array));
		System.out.println(getIndexOfTheFirstMinNumber(array));
		System.out.println(computeTotalOfMaxNumber(array));
		System.out.println(computeTotalOfMinNumber(array));
	}
}
