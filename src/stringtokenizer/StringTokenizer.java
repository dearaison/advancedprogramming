package stringtokenizer;

public class StringTokenizer {
	String origin; // The string after remove delimiter;
	String delimiter;

	public StringTokenizer(String line, String delimiter) {
		this.delimiter = delimiter;
		origin = removeDelimiter(line);
	}

	boolean hasMoreToken() {
		return !origin.isEmpty();
	}

	String removeDelimiter(String line) {
		int len = delimiter.length();
		while (line.startsWith(delimiter))
			line = line.substring(len);
		while (line.endsWith(delimiter))
			line = line.substring(0, line.length() - len);
		return line;
	}
	
	String nextToken() {
		String res;
		if (origin.indexOf(delimiter) < 0) {
			res = origin;
			origin = "";
		} else {
			res = origin.substring(0, origin.indexOf(delimiter));
			origin = removeDelimiter(origin.substring(origin.indexOf(delimiter)));
		}
		return res;
	}

	public static void main(String[] args) {
		StringTokenizer s1 = new StringTokenizer("ABAB123AB456ABAB789AB", "AB");
		System.out.println(s1.removeDelimiter("ABAB123AB456ABAB789AB"));
		while (s1.hasMoreToken())
			System.out.println(s1.nextToken());
	}

}
