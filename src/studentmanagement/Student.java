package studentmanagement;

public class Student {
	String id, name;
	float averageScore;

	public Student(String id, String name, double averageScore) {
		super();
		this.id = id;
		this.name = name;
		this.averageScore = (float) averageScore;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getAverageScore() {
		return averageScore;
	}

	public void setAverageScore(float averageScore) {
		this.averageScore = averageScore;
	}

}
