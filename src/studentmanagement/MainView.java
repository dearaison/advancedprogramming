package studentmanagement;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

public class MainView extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JFrame MyFrame;
	JPanel panel12, panel3;
	JPanel enterInfor, operator, board;
	JLabel name, iD, avgScore;
	JTextField enterID, enterName, enterAverageScore;
	JButton add, delete, change, find;
	JLabel showingID, showingName, showingAvgScore;
	ArrayList<Student> list;

	public MainView() {
		list = new ArrayList<>();
		list.add(new Student("15130210", "Nguyễn Ng�?c Lâm Bảo Trư�?ng", 9.0));

		enterInfor = new JPanel();
		setUpEnterInfor();

		operator = new JPanel();
		setUpOperator();

		board = new JPanel();
		setUpBoard();

		JScrollPane scroll = new JScrollPane(board);

		panel12 = new JPanel();
		panel12.setLayout(new BorderLayout());
		panel12.add(enterInfor, BorderLayout.NORTH);
		panel12.add(operator, BorderLayout.SOUTH);

		panel3 = new JPanel();
		panel3.setLayout(new BorderLayout());
		panel3.add(panel12, BorderLayout.NORTH);
		panel3.add(scroll, BorderLayout.CENTER);

		MyFrame = new JFrame("Quản lý sinh viên");

		MyFrame.getContentPane().setLayout(new BorderLayout());
		MyFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		MyFrame.getContentPane().add(panel3, BorderLayout.NORTH);

		MyFrame.pack();
		MyFrame.setLocationRelativeTo(null);
		MyFrame.setVisible(true);

	}

	private void setUpBoard() {
		board.removeAll();
		board.setBorder(BorderFactory.createTitledBorder("Danh sách sinh viên"));
		board.setLayout(new GridLayout(0, 3));

		showingID = new JLabel("MSSV");
		board.add(showingID);

		showingName = new JLabel("H�? và Tên");
		board.add(showingName);

		showingAvgScore = new JLabel("�?iểm TB");
		board.add(showingAvgScore);

		for (Student student : list) {
			board.add(new JLabel(student.id));
			board.add(new JLabel(student.name));
			board.add(new JLabel(student.averageScore + ""));
		}
	}

	private void setUpOperator() {
		operator.setBorder(BorderFactory.createTitledBorder("Thao tác"));
		operator.setLayout(new FlowLayout());

		add = new JButton("Thêm");
		add.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String newID = enterID.getText();
				String newName = enterName.getText();
				float newAvg = Float.parseFloat(enterAverageScore.getText());
				addSV(newID, newName, newAvg);
			}
		});
		operator.add(add);

		delete = new JButton("Xóa");
		operator.add(delete);
		delete.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String id = enterID.getText().trim();
				delete(id);

			}
		});

		change = new JButton("Sửa");
		operator.add(change);
		change.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String id = enterID.getText().trim();
				changeInformations(id);
			}
		});

		find = new JButton("Tìm");
		find.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String id = enterID.getText();
				if (find(id) != (null)) {
					String studentID = find(id).id + "";
					enterID.setText(studentID);
					enterName.setText(find(id).name);
					@SuppressWarnings("unused")
					String studentAvgScore;
					enterAverageScore.setText(studentAvgScore = "" + find(id).averageScore);
					board.revalidate();
				}

			}
		});
		operator.add(find);
	}

	private void setUpEnterInfor() {
		enterInfor.setBorder(BorderFactory.createTitledBorder("TTSV"));
		enterInfor.setLayout(new GridLayout(3, 2));

		iD = new JLabel("MSSV", JLabel.RIGHT);
		enterID = new JTextField();

		name = new JLabel("H�? và tên", JLabel.RIGHT);
		enterName = new JTextField();

		avgScore = new JLabel("�?iểm TB", JLabel.RIGHT);
		enterAverageScore = new JTextField();

		enterInfor.add(iD);
		enterInfor.add(enterID);
		enterInfor.add(name);
		enterInfor.add(enterName);
		enterInfor.add(avgScore);
		enterInfor.add(enterAverageScore);
	}

	public Student find(String id) {
		String id1 = id.trim();
		for (Student student : list) {
			if (student.id.equalsIgnoreCase(id1))
				return student;
		}
		return null;
	}

	public void addSV(String id, String name, double avgScoore) {
		String id1 = id.trim();
		String name1 = name.trim();
		if (find(id1) == null) {
			list.add(new Student(id1, name1, avgScoore));
			setUpBoard();
			pack();
		}
	}

	public void delete(String id) {
		String id1 = id.trim();
		Student st = null;
		if ((st = find(id1)) != null) {
			list.remove(st);
			setUpBoard();
			pack();

		}
	}

	public void changeInformations(String id) {
		String id1 = id.trim();
		String name = enterName.getText().trim();
		float avgScore = Float.parseFloat(enterAverageScore.getText());
		Student st = null;
		if ((st = find(id1)) != null) {
			st.setName(name);
			st.setAverageScore(avgScore);
			setUpBoard();
			revalidate();
		}
	}

	public static void main(String[] args) {
		new MainView();
	}

}
