package testanddemo;

import java.util.ArrayList;

public class Test1 {
	public boolean test1a(int n) {
		// check n is natural number or not?
		if (n < 0) {
			return false;
		} else {
			if (n == 2 || n == 3) {
				return true;
			} else if (n < 2) {
				return false;
			} else {
				for (int temp = 2; temp < n - 1; temp++) {
					if (n % temp == 0) {
						return false;
					}
				}
				return true;
			}
		}

	}

	public ArrayList<Integer> test1b(int n) {
		ArrayList<Integer> res = new ArrayList<>();
		if (n < 0) {
			return res;
		} else {
			while (test1a(n) == false) {
				for (int i = 2; i < n - 1; i++) {
					if (test1a(i) == true && n % i == 0) {
						res.add(i);
						n = n / i;
					}
				}
			}
		}
		res.add(n);
		return res;

	}

	public boolean test2(ArrayList<Integer> arrayList) {
		for (int index = 0; index < arrayList.size() - 1; index++) {
			if (arrayList.get(index) > 0 && arrayList.get(index + 1) > 0
					|| arrayList.get(index) < 0 && arrayList.get(index + 1) < 0)
				return false;

		}
		return true;

	}

	public void printMatrix(int[][] arr) {
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				System.out.print(arr[i][j]);
			}
			System.out.println();
		}
	}

	public boolean compareMatirx(int[][] arr) {
		if (arr.length != arr[0].length) {
			return false;
		} else {
			int size = arr.length;
			for (int i = 0; i < size; i++) {
				for (int j = 0; j < size; j++) {
					int temp1 = arr[i][j];
					int temp2 = arr[j][i];
					if (temp1 != temp2) {
						return false;
					}

				}

			}
			return true;
		}
	}

	public String encryptAString(String origin) {
		String res = "";
		int temp1 = 'a';
		int temp2 = 'z';
		int temp3 = temp2 - temp1 + 1;
		String[] fromaToz = new String[temp3];
		String[] fromAToZ = new String[temp3];
		String[] from0To9 = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
		for (int i = temp1, i1 = 0; i <= temp2 && i1 < fromaToz.length; i++, i1++) {
			char temp4 = (char) i;
			fromaToz[i1] = temp4 + "";
		}
		for (int i = 0; i < fromaToz.length; i++) {
			fromAToZ[i] = fromaToz[i].toUpperCase();
		}
		String temp5 = "";
		String temp6 = "";
		for (int i = 0; i < origin.length(); i++) {
			temp5 = origin.charAt(i) + "";
			int j = 0;
			int k = 0;
			while (j < fromaToz.length || k < from0To9.length) {
				if (temp5.equals(from0To9[k])) {
					temp6 = from0To9[k - 2];
				} else {
					if (temp5.equals(fromAToZ[j])) {
						temp6 = fromAToZ[j - 2];
					} else {
						if (temp5.equals(fromaToz[j])) {
							temp6 = fromaToz[j - 2];
						} else {
							temp6 = temp5;
						}

					}
				}

				if (j < fromaToz.length-1)
					j++;
				if (k < from0To9.length-1){
					k++;}
			}
			res += temp6;
		}
		return res;

	}

	public static void main(String[] args) {
		Test1 test = new Test1();
		// int[] temp = { 1, -2, 3, -96, 0, 0, 65, -9, 69, -56, 79 };
		// ArrayList<Integer> temp2 = new ArrayList<>();
		// for (int i = 0; i < temp.length; i++)
		// temp2.add(temp[i]);
		// System.out.println(test.test2(temp2));
		// int n = 555;
		// test.test1a(n);
		// System.out.println(test.test1b(n));
		//
		// int[][] a = { { 1,2,3,8 }, { 2,5,6,7 }, { 3,6,9,9 }, { 8,7,9,5 } };
		// System.out.println(test.compareMatirx(a));
		System.out.println(test.encryptAString("-c*d/@C#D%&3*9(8)7_="));
	}
}
