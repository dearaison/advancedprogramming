package testanddemo;

import java.util.*;

public class TempTest1 {
	public String e(String x) {
		String res = null;
		switch (x) {
		case "1":
			res = "mot";
			break;
		case "2":
			res = "hai";
			break;
		case "3":
			res = "ba";
			break;
		case "4":
			res = "bon";
			break;
		case "5":
			res = "nam";
			break;
		case "6":
			res = "sau";
			break;
		case "7":
			res = "bay";
			break;
		case "8":
			res = "tam";
			break;
		case "9":
			res = "mot";
			break;
		case "0":
			res = "khong";
			break;

		default:
			break;
		}
		return res;

	}

	public String b(String a) {
		String temp1;
		String temp2;
		temp1 = (a.substring(0, 1)).toUpperCase();
		temp2 = a.substring(1);
		return temp1 + temp2;

	}

	public void def() {
		String temp;
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number of testcase: ");
		int m = sc.nextInt();
		String[] res = new String[m];
		for (int i = 0; i < res.length; i++) {
			System.out.print("Enter a number : ");
			int aNumber = sc.nextInt();
			String numString = aNumber + "";
			int numLength = numString.length();
			switch (numLength) {
			case 1: {
				temp = e(numString);
				res[i] = b(temp);
				break;
			}
			case 2: {
				String first = numString.substring(0, 1);
				String second = numString.substring(1);
				if (Integer.parseInt(first) < 2) {
					if (Integer.parseInt(second) < 1) {
						res[i] = b("muoi");
					}
					if (Integer.parseInt(second) > 0) {
						temp = "muoi " + e(second);
						res[i] = b(temp);
					}
				} else {
					if (Integer.parseInt(first) > 1) {
						if (Integer.parseInt(second) < 1) {
							temp = e(first) + " muoi";
							res[i] = b(temp);
						}
						if (Integer.parseInt(second) > 0) {
							temp = e(first) + " muoi " + e(second);
							res[i] = b(temp);
						}
					}
				}
				break;
			}
			case 3: {
				String first = numString.substring(0, 1);
				String second = numString.substring(1, 2);
				String third = numString.substring(2);
				if (Integer.parseInt(second) < 2 && Integer.parseInt(second) > 0) {
					if (Integer.parseInt(third) < 1) {
						temp = e(first) + "tram muoi";
						res[i] = b(temp);
					}
					if (Integer.parseInt(third) > 0) {
						temp = e(first) + " tram muoi " + e(third);
						res[i] = b(temp);
					}
				} else {
					if (Integer.parseInt(second) > 1) {
						if (Integer.parseInt(third) < 1) {
							temp = e(first) + " tram " + e(second) + " muoi";
							res[i] = b(temp);
						}
						if (Integer.parseInt(third) > 0) {
							temp = e(first) + " tram " + e(second) + " muoi " + e(third);
							res[i] = b(temp);
						}
					} else {
						if (Integer.parseInt(second) < 1) {
							if (Integer.parseInt(third) < 1) {
								temp = e(first) + " tram ";
								res[i] = b(temp);
							}
							if (Integer.parseInt(third) > 0) {
								temp = e(first) + " tram linh " + e(third);
								res[i] = b(temp);
							}
						}

					}
				}

				break;
			}
			default:
				break;
			}
		}
		for (int i = 0; i < res.length; i++) {
			System.out.println("Testcase " + i + ": " + res[i]);
		}
		sc.close();
	}

	public static void main(String[] args) {
		new TempTest1().def();

	}

}
