package testanddemo;

import java.util.Random;
import java.util.Scanner;

public class Test4 {

	public int[] name() {
		int a;
		Scanner sc = new Scanner(System.in);
		while (true) {
			System.out.print("enter a number higher than 400,000: ");
			a = sc.nextInt();
			if (a < 400000)
				break;
		}
		Random ran = new Random();
		int[] res = new int[a];
		for (int i = 0; i < res.length; i++) {
			int temp = ran.nextInt(a + 1);
			res[i] = temp;

		}
		sc.close();
		return res;
	}

	public void print(int[] a) {
		for (int i : a) {
			System.out.print(i + "\t");

		}
	}

	public static void main(String[] args) {
		Test4 a = new Test4();
		a.print(a.name());

	}

}
