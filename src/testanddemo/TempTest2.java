package testanddemo;

import java.util.Scanner;

public class TempTest2 {
	public void abc() {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number of testcase: ");
		int m = sc.nextInt();
		int[] res = new int[m];
		for (int i = 0; i < res.length; i++) {
			System.out.print("Enter f0: ");
			int f0 = sc.nextInt();
			System.out.print("Enter f1: ");
			int f1 = sc.nextInt();
			System.out.print("Enter n: ");
			int n = sc.nextInt();
			int temp;
			for (int j = 2; j <= n; j++) {
				temp = f0;
				f0 = f1;
				f1 = 2 * f0 - temp;
			}
			res[i] = f1;
		}
		for (int i = 0; i < res.length; i++) {
			System.out.println("Testcase " + i + ": " + res[i]);

		}
		sc.close();
	}
	public static void main(String[] args) {
		new TempTest2().abc();
	}

}
