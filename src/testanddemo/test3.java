package testanddemo;

public class test3 {
	public static void main(String[] args) {
		test3 test = new test3();
		int[] arr = { 9, 5, 3, 10, 8, 20 };
		int[][] a = test.test2(arr);
		test.print2Dmatrix(a);
	}

	void print2Dmatrix(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				System.out.print(matrix[i][j] + " ");

			}
			System.out.println();
		}

	}

	public int[][] test2(int[] arr) {
		int row = arr.length;
		int col = row;
		int[][] newArr = new int[row][col];
		for (int i = newArr.length - 1, j = 0, z = 0; i >= 0 && z < arr.length && j < newArr[i].length; i--, j++, z++) {
			newArr[i][j] = arr[z];

		}
		for (int i = newArr.length - 1, j = 0; i >= 0 && j < newArr[i].length; i--, j++) {
			int temp = newArr[i][j];
			for (int k = j + 1; k < newArr.length; k++) {
				newArr[i][k] = ++temp;
				// newArr[i][k] = newArr[i][k - 1] + 1;
			}
		}
		for (int i = 0, j = newArr[i].length - 1; j >= 0 && i < newArr[i].length; i++, j--) {
			int temp = newArr[i][j];
			for (int k = j - 1; k >= 0; k--) {
				newArr[i][k] = --temp;
				// newArr[i][k] = newArr[i][k + 1] - 1;
			}
		}
		return newArr;

	}
}
