package testanddemo;

public class BubleSorting {
	public int[][] sorting2DArray(int[][] arr) {
		int totalLength = 0;
		for (int i = 0; i < arr.length; i++) {
			totalLength += arr[i].length;
		}
		int[] tempArray = new int[totalLength];
		for (int i = 0, i2 = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++, i2++) {
				tempArray[i2] = arr[i][j];
			}
		}
		sorting1DArray(tempArray);
		for (int i = 0, i2 = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++, i2++) {
				arr[i][j] = tempArray[i2];
			}
		}
		return arr;
	}
	
	// We will find lower numbers and put it in order of array.
	public int[] sorting1DArray(int[] arr) {
		for (int i = 0; i < arr.length - 1; i++) {
			for (int j = i + 1; j < arr.length; j++) {
				if (arr[j] < arr[i]) {
					int temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}
		}
		return arr;
	}

	public void print2DMatrix(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				System.out.print(matrix[i][j] + " ");

			}
			System.out.println();
		}

	}

	public static void main(String[] args) {
		int[][] test = {{7,5,9},{4,8,2},{0,1,3}};
		BubleSorting testClass = new BubleSorting();
		testClass.print2DMatrix(testClass.sorting2DArray(test));
	}

}
