package testanddemo;

public class MultipleThreadDemo implements Runnable {
	// Second way: with second way we implements Runnable interface then write
	// run method to have a multiple thread.

	@Override
	public void run() {
		// TODO Auto-generated method stub
		for (int i = 0; i < 100; i++) {
			System.out.print(i);
		}
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// First way: with this way we extend Thread class and write run method to
	// have
	// multiple thread.

	// public void run() {
	// for (int i = 0; i < 5; i++) {
	// System.out.print(i);
	// }
	// }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// First way.
		// MultipleThread th1 = new MultipleThread();
		// MultipleThread th2 = new MultipleThread();
		// th1.start();
		// th2.start();

		// Second way.
		Thread th1 = new Thread(new MultipleThreadDemo());
		Thread th2 = new Thread(new MultipleThreadDemo());
		Thread th3 = new Thread(new MultipleThreadDemo());
		Thread th4 = new Thread(new MultipleThreadDemo());
		Thread th5 = new Thread(new MultipleThreadDemo());
		th1.start();
		th2.start();
		th3.start();
		th4.start();
		th5.start();
	}

}
