package integer;

public class Integer {
	public static void main(String[] args) {
		int x = 53;
		System.out.println(new Integer().isOdd(x));
		System.out.println(new Integer().isPrime(x));
	}

	// to check x is odd
	public boolean isOdd(int x) {
		if (x % 2 == 0)
			return false;
		return true;
	}

	// to check x is Elemental integer;
	public boolean isPrime(int x) {
		if (x == 2 || x == 3)
			return true;
		else if (x < 2)
			return false;
		else
			for (int i = 2; i <= x - 1; i++) {
				if (x % i == 0)
					return false;
			}
		return true;
	}

}
