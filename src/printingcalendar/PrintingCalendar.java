package printingcalendar;

public class PrintingCalendar {

	public void printCalendar(byte beginDayOfMonth, byte numberOfDayInMonth) {
		// tao ra mot mang de hien thi ten cac ngay trong tuan, roi in mang do
		// ra
		// bien tempDay de tam thoi luu tru ngay cua thang.
		// bien temp dung de tam thoi luu tru chuoi se gan vao vi tri
		// [i][j], mac dinh la chuoi rong:""
		// cua mang day.
		// bien count dung de xac dinh khi nao thi bat dau gan gia tri
		// cua bien tempDay vao bien temp, va sau moi lan gan se tang
		// bien tempDay len 1 don vi.
		// Y tuong: khi truy xuat qua tung phan tu cua mang day thi bien
		// count se tang len 1 don vi, khi gia tri cua bien count bang
		// voi bien beginDayOfMonth thi bat dau gan tempDay vao temp roi
		// tang tempDay len 1 don vi cho toi khi nao tempDay > so ngay
		// trong thang thi ngung.
		String[] dayName = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
		for (String day : dayName) {
			System.out.print(day + " ");
		}
		System.out.println();
		String[][] day = new String[5][7];
		for (int i = 0, count = 1, tempDay = 1; i < 5; i++) {
			for (int j = 0; j < 7; j++, count++) {

				String temp = "";
				if (count >= beginDayOfMonth && tempDay <= numberOfDayInMonth) {
					temp += tempDay;
					tempDay++;
				}
				day[i][j] = temp;
			}
		}
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 7; j++) {
				System.out.print(day[i][j]);
				System.out.print("\t" + "    ");
			}
			System.out.println();
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PrintingCalendar abc = new PrintingCalendar();
		byte num1 = 3;
		byte num2 = 31;
		abc.printCalendar(num1, num2);

	}
}
