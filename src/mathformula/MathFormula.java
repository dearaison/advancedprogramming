package mathformula;

public class MathFormula {
	public static void main(String[] args) {
		System.out.println(computeFirstFormula(2, 5));
		System.out.println(computeSecondFormula(2, 5));
		System.out.println(computeN(5));
		System.out.println(computeThirdFormula(2, 5));
	}

	static int computeFirstFormula(double x, int n) {
		int res = 0;
		for (int exp = 0; exp <= n; exp++)
			res += Math.pow(x, exp);
		return res;
	}

	static int computeSecondFormula(double x, int n) {
		int res = 0;
		byte sign = -1;
		for (int exp = 0; exp <= n; exp++)
			res += ((sign *= -1) * Math.pow(x, exp));
		return res;
	}

	static int computeN(int n) {
		int res = 1;
		for (int i = 1; i <= n; i++)
			res *= i;
		return res;
	}

	static double computeThirdFormula(double x, int n) {
		double res = 0;
		for (int exp = 0; exp <= n; exp++)
			res += (Math.pow(x, exp)/computeN(exp));
		return res;
	}

}
