package paint;

import java.awt.Graphics;

public abstract class AShape {
	int x;
	int y;
	public AShape(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}
	abstract void drawAShape(Graphics g);
	abstract void resize(int x2, int y2);

}
