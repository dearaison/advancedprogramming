package paint;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class MyJFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MyJFrame() {
		setTitle("pane");
		JPanel pane = new MyJPanel();
		getContentPane().add(pane);

		setSize(500, 500);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

	public static void main(String[] args) {
		new MyJFrame();
	}

}
