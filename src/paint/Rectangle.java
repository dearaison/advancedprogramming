package paint;

import java.awt.Graphics;

public class Rectangle extends AShape {
	int x2;
	int y2;

	public Rectangle(int x, int y) {
		super(x, y);
		x2 = x;
		y2 = y;
	}

	@Override
	void drawAShape(Graphics g) {
		g.drawRect(x, y, x2 - x, y2 - y);

	}

	int swap1 = this.x, swap2 = this.y;

	@Override
	void resize(int x2, int y2) {
		if (x2 < x || y2 < y) {
			this.x = x2;
			this.y = y2;
		} else {
			this.x = swap1;
			this.y = swap2;
			this.x2 = x2;
			this.y2 = y2;
		}
	}
}