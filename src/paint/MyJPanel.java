package paint;

import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JPanel;

public class MyJPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int NO_SHAPE_SELECTED = 0;
	public static final int LINE_SELECTED = 1;
	public static final int RECTANGLE_SELECTED = 2;
	public static final int OVAL_SELECTED = 3;
	private int selectedShape = NO_SHAPE_SELECTED;
	private boolean start;
	private AShape last;
	ArrayList<AShape> shapes;

	public MyJPanel() {
		setFocusable(true);
		start = false;
		shapes = new ArrayList<AShape>();
		
//		JColorChooser a = new JColorChooser();
//		add(a);

		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				super.keyPressed(e);
				if (e.isControlDown()) {
					int key = e.getKeyCode();
					switch (key) {
					case KeyEvent.VK_L:
						selectedShape = LINE_SELECTED;
						System.out.println("Line");
						break;
					case KeyEvent.VK_R:
						selectedShape = RECTANGLE_SELECTED;
						System.out.println("Rectangle");
						break;
					case KeyEvent.VK_O:
						selectedShape = OVAL_SELECTED;
						System.out.println("Oval");
						break;
					default:
						break;
					}
				}
			}
		});
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				super.mouseClicked(e);
				if (selectedShape != NO_SHAPE_SELECTED) {
					int key = e.getButton();
					switch (key) {
					case MouseEvent.BUTTON1: {
						start = true;
						switch (selectedShape) {
						case LINE_SELECTED:
							last = new Line(e.getX(), e.getY());
							shapes.add(last);
							break;
						case RECTANGLE_SELECTED:
							last = new Rectangle(e.getX(), e.getY());
							shapes.add(last);
							break;
						case OVAL_SELECTED:
							last = new Oval(e.getX(), e.getY());
							shapes.add(last);
							break;
						default:
							break;
						}
					}
						break;

					case MouseEvent.BUTTON3:
						start = false;

					default:
						break;
					}

				}
			}
		});
		addMouseMotionListener(new MouseAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				// TODO Auto-generated method stub
				super.mouseMoved(e);
				if ((selectedShape != NO_SHAPE_SELECTED) && (start)) {
					AShape last = shapes.get(shapes.size() - 1);
					last.resize(e.getX(), e.getY());
					shapes.add(last);
				}
				repaint();
			}
		});
	}

	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);
		if (shapes.size() < 1)
			return;
		for (AShape aShape : shapes) {
			aShape.drawAShape(g);
		}
	}
}
/*
 * jcolorchooser
 */
