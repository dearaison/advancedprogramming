package paint;

import java.awt.Graphics;

public class Line extends AShape {
	int x2;
	int y2;

	public Line(int x, int y) {
		super(x, y);
		x2 = x;
		y2 = y;
	}

	@Override
	void drawAShape(Graphics g) {
		g.drawLine(x, y, x2, y2);

	}

	@Override
	void resize(int x2, int y2) {
		this.x2 = x2;
		this.y2 = y2;

	}


}
