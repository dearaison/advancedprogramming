package freefalling;

public class Falling {
	public static void main(String[] args) {
		double high = 5;
		System.out.println(new Falling().toComputeTime(high));
		System.out.println(new Falling().toComputeSpeed(high));

	}

	// to compute the time of falling in second
	public double toComputeTime(double high) {
		if (high > 0)
			return Math.sqrt((2 * high) / 10);
		return 0;

	}

	// to compute the falling speed when thing down to the ground in meter per
	// second
	public double toComputeSpeed(double high) {
		return 10 * toComputeTime(high);

	}
}
