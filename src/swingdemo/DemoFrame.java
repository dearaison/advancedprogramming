package swingdemo;

import java.awt.*;
import javax.swing.*;

public class DemoFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Point point = new Point(0, 0);
	JPanel pane;

	public DemoFrame() {
		Container contentPane = getContentPane();
		contentPane.add(new PanelDemo());

		setSize(500, 500);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);

	}

	public static void main(String[] args) {
		new DemoFrame();
	}

}
