package swingdemo;

import java.awt.event.KeyEvent;

import javax.swing.*;

public class MenuDemo extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public MenuDemo() {
		// TODO Auto-generated constructor stub
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu fileMenu = new JMenu("File");
		fileMenu.setMnemonic(KeyEvent.VK_F);
		menuBar.add(fileMenu);
		
		JMenuItem open = new JMenuItem("Open");
		open.setMnemonic(KeyEvent.VK_O);
		fileMenu.add(open);
		
		setTitle("Demo");
		pack();
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new MenuDemo();

	}

}
