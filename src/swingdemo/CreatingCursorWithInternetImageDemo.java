package swingdemo;

//This demo need connecting to Internet to get image which is use as custom
//cursor.
import java.awt.*;
import java.net.*;
import javax.swing.*;

public class CreatingCursorWithInternetImageDemo {

	protected void initUI() throws MalformedURLException {
		JFrame frame = new JFrame("Test text pane");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Image image = toolkit
				.getImage(new URL("http://fc03.deviantart.net/fs71/f/2010/094/7/9/Kalyan_hand_cursor_1_by_Zanten.png"));
		Point hotspot = new Point(0, 0);
		Cursor cursor = toolkit.createCustomCursor(image, hotspot, "Stone");
		frame.setCursor(cursor);
		
		JOptionPane.showMessageDialog(null, new ImageIcon(new URL("http://orig11.deviantart.net/2a05/f/2009/187/e/4/glaceon_cursor_by_lionheartcartoon.gif")));
		
		frame.setSize(600, 400);
		frame.setVisible(true);
		
		//http://img11.deviantart.net/e8b8/i/2012/298/5/b/pinkie_pie_cursor_set_by_kirigakurenohaku-d4cqepx.gif
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				try {
					new CreatingCursorWithInternetImageDemo().initUI();
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}
}
