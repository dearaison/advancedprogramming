package swingdemo;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.*;

public class CheckBoxComboBoxAndRadiusButtonDemo extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// JCheckBox javaChoice, cChoice, pascalChoice;
	JRadioButton javaChoice, cChoice, pascalChoice;
	JComboBox<String> comboBox;
	JPanel input, output;
	JLabel message;

	public CheckBoxComboBoxAndRadiusButtonDemo() {
		// TODO Auto-generated constructor stub

		 String[] programmingLanguageList = { "Java", "C", "Pascal" };
		 comboBox = new JComboBox<>(programmingLanguageList);
		 comboBox.addActionListener(myListenner);

		// Create check box
		// javaChoice = new JCheckBox("java");
		// javaChoice.setActionCommand("Java");
		// javaChoice.addActionListener(myListenner);
		//
		// cChoice = new JCheckBox("C");
		// cChoice.setActionCommand("C");
		// cChoice.addActionListener(myListenner);
		//
		// pascalChoice = new JCheckBox("Pascal");
		// pascalChoice.setActionCommand("Pascal");
		// pascalChoice.addActionListener(myListenner);

		// Create radio button
		javaChoice = new JRadioButton("java");
		javaChoice.setActionCommand("Java");
		javaChoice.addActionListener(myListenner);

		cChoice = new JRadioButton("C");
		cChoice.setActionCommand("C");
		cChoice.addActionListener(myListenner);

		pascalChoice = new JRadioButton("Pascal");
		pascalChoice.setActionCommand("Pascal");
		pascalChoice.addActionListener(myListenner);

		// group radiusButton
		ButtonGroup selectGroup = new ButtonGroup();
		selectGroup.add(javaChoice);
		selectGroup.add(cChoice);
		selectGroup.add(pascalChoice);
		javaChoice.setSelected(true);

		// add Mmenonic
		javaChoice.setMnemonic(KeyEvent.VK_J);
		cChoice.setMnemonic(KeyEvent.VK_C);
		pascalChoice.setMnemonic(KeyEvent.VK_P);

		input = new JPanel();
		input.setBorder(BorderFactory.createTitledBorder("Programming Language:"));
		 input.add(comboBox);
//		input.add(javaChoice);
//		input.add(cChoice);
//		 input.add(pascalChoice);

		output = new JPanel();
		output.setBorder(BorderFactory.createTitledBorder("Output"));
		message = new JLabel("", JLabel.CENTER);
		output.add(message);

		Container contentpane = getContentPane();
		contentpane.setLayout(new BoxLayout(contentpane, BoxLayout.Y_AXIS));
		contentpane.add(input);
		contentpane.add(output);

		setTitle("CheckBox");
		pack();
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

	ActionListener myListenner = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			String text;
			// JCheckBox cb = (JCheckBox) e.getSource();
			 @SuppressWarnings("unchecked")
			JComboBox<String> cb = (JComboBox<String>) e.getSource();
//			JRadioButton cb = (JRadioButton) e.getSource();
			// if (cb.isSelected()) {
//			text = e.getActionCommand() + " is selected.";
			// } else {
			// text = e.getActionCommand() + " is unselected.";
			// }
			 text = "";
			 String item = (String) cb.getSelectedItem();
			 if(item != null) text = item + " is selected";
			message.setText(text);
			pack();
		}
	};

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new CheckBoxComboBoxAndRadiusButtonDemo();

	}

}
