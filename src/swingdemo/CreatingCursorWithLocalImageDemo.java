package swingdemo;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import javax.imageio.*;
import javax.swing.*;

// This demo can run without connecting to Internet.
public class CreatingCursorWithLocalImageDemo extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JButton button1, button2;
	Cursor cur1, cur2;

	public CreatingCursorWithLocalImageDemo() throws IOException {
		// TODO Auto-generated constructor stub
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);

		Toolkit toolkit = Toolkit.getDefaultToolkit();

		// This way is't optimal, because when the program runs on others device
		// absolute path of file may be changed. Then, the image will be
		// invalid.
		// File cursorFile = new
		// File("B:\\baotr\\workspace\\LTNC\\src\\testanddemo\\cursor.jpg");
		// Image imag = toolkit.getImage(cursorFile.getAbsolutePath());
		// /*toolkit.getImage("B:\\baotr\\workspace\\LTNC\\src\\testanddemo\\cursor.jpg");*/

		// First way
		Image imag1 = toolkit.createImage(getClass().getResource("cur1.png"));
		cur1 = toolkit.createCustomCursor(imag1, new Point(0, 0), "a");

		// Second way
		Image imag2 = new ImageIcon(ImageIO.read(getClass().getResource("anicur.gif"))).getImage();
		cur2 = toolkit.createCustomCursor(imag2, new Point(0, 0), "b");

		ImageIcon icon1 = new ImageIcon(ImageIO.read(getClass().getResource("cur1.png")));
		button1 = new JButton(icon1);
		button1.addActionListener(myListener);
		button1.setActionCommand("curs1");

		ImageIcon icon2 = new ImageIcon(ImageIO.read(getClass().getResource("anicur.gif")));
		button2 = new JButton(icon2);
		button2.addActionListener(myListener);
		button2.setActionCommand("curs2");

		JOptionPane.showMessageDialog(null, icon2);
		JPanel myPanel = new JPanel();
		myPanel.setLayout(new BoxLayout(myPanel, BoxLayout.Y_AXIS));
		myPanel.add(button1);
		myPanel.add(button2);
		this.add(myPanel);
		pack();
		setLocationRelativeTo(null);
	}

	ActionListener myListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			String cur = e.getActionCommand();
			switch (cur) {
			case "curs1":
				setCursor(cur1);
				break;
			case "curs2":
				setCursor(cur2);
				break;
			default:
				break;
			}
		}
	};

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			new CreatingCursorWithLocalImageDemo();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
