package swingdemo;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JPanel;

public class PanelDemo extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean start;
	// public static final int NORMAL_INCREATMENT = 5;
	// public static final int LARGE_INCREATMENT = 100;
	Point last;
	ArrayList<Point> line;

	public PanelDemo() {
		start = false;
		setFocusable(true);
		line = new ArrayList<>();
		// last = new Point(10, 10);
		// line.add(last);
		// addKeyListener(new KeyAdapter() {
		// @Override
		// public void keyPressed(KeyEvent e) {
		// // TODO Auto-generated method stub
		// int delta;
		// super.keyPressed(e);
		// if (e.isShiftDown()) {
		// delta = LARGE_INCREATMENT;
		// } else {
		// delta = NORMAL_INCREATMENT;
		// }
		// int key = e.getKeyCode();
		// switch (key) {
		// case KeyEvent.VK_UP:
		// add(0, -delta);
		// break;
		// case KeyEvent.VK_DOWN:
		// add(0, delta);
		// break;
		// case KeyEvent.VK_RIGHT:
		// add(delta, 0);
		// break;
		// case KeyEvent.VK_LEFT:
		// add(-delta, 0);
		// break;
		// default:
		// break;
		// }
		//
		// }
		//
		// });
		addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				super.mouseClicked(e);
				last = new Point(e.getX(), e.getY());
				line.add(last);
				start = true;
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				super.mouseReleased(e);
				last = new Point(e.getX(), e.getY());
				line.add(last);
				start = false;
			}
		});
		
		addMouseMotionListener(new MouseAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				// TODO Auto-generated method stub
				super.mouseMoved(e);
				if (start) {
					last = new Point(e.getX(), e.getY());
					line.add(last);
					repaint();
				}
			}
		});

	}

	// public void add(int xDelta, int yDelta) {
	// Point end = new Point(last.x + xDelta, last.y + yDelta);
	// line.add(end);
	// repaint();
	// last = end;
	// }

	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);
		if (line.size() < 2)
			return;
		for (int i = 1; i < line.size(); i++) {
			g.drawLine(line.get(i - 1).x, line.get(i - 1).y, line.get(i).x, line.get(i).y);
		}

	}
	

}
