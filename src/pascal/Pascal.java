package pascal;

public class Pascal {
	public static void main(String[] args) {
		new Pascal().printPascalTraingle1(5);
	}

	void printPascalTraingle1(int size) {
		int[][] array = new int[size][];
		for (int i = 0; i < array.length; i++) {
			array[i] = new int[i + 1]; // to creat sub-array
		}
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
				array[i][j] = 1 + j; // to add munber to array
			}
		}
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
				System.out.print(array[i][j] + " ");
			}
			System.out.println(); // to print a pacal traingle
		}

	}

	void printPascalTraingle2(int size) {
		int[][] array = new int[size][];
		for (int i = 0; i < array.length; i++) {
			array[i] = new int[i + 1]; // to creat sub-array
		}
		for (int i = 0; i < array.length; i++) {
			array[i][0] = 1;
			array[i][i] = 1;
			for (int j = 1; j < array[i].length-1; j++) {
				array[i][j] = array[i - 1][j - 1] + array[i - 1][j]; // to add munber to array
			}
		}
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
				System.out.print(array[i][j] + "\t");
			}
			System.out.println(); // to print a pacal traingle
		}

	}
}
