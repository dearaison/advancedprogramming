package pascal;

public class The2DMatrix {
	public static void main(String[] args) {
		int[][] aMatrix = { { 1, 2, 3 },
							{ 6, 5, 9, 0 },
							{ 2, 7 },
							{3,6,8,4,5,6,7,3,0,1} };

		new The2DMatrix().print2Dmatrix(aMatrix);
	}

	void print2Dmatrix(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				System.out.print(matrix[i][j] + " ");

			}
			System.out.println();
		}

	}

}
