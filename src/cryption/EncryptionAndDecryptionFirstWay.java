package cryption;

public class EncryptionAndDecryptionFirstWay {

	public static void main(String[] args) {
		int n = 2;
		String txt = "I am Student";
		String a = encrypt(n, txt);
		System.out.println(a);
		System.out.println(decrypt(n, a));
		System.out.println(decryptByAnotheWay(n, a));
	}

	static String encrypt(int n, String txt) {
		String res = "";
		int col = n;
		int row = txt.length() / n;
		row += (txt.length() % n) > 0 ? 1 : 0;
		char[][] array = new char[row][col];
		for (int i = 0, index = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				char temp = '-';
				if (index < txt.length()) {
					char tempTxt = txt.charAt(index);
					if (tempTxt != ' ') {
						temp = tempTxt;
					}
				}
				array[i][j] = temp;
				index++;
			}
		}
		for (int j = 0; j < col; j++) {
			for (int i = 0; i < row; i++) {
				res += array[i][j];
			}
		}
		return res;
	}

	static String decrypt(int n, String txt) {
		String res = "";
		int col = n;
		int row = txt.length() / n;
		row += (txt.length() % n) > 0 ? 1 : 0;
		char[][] array = new char[row][col];
		for (int j = 0, index = 0; j < col; j++) {
			for (int i = 0; i < row; i++) {
				char temp = ' ';
				if (index < txt.length()) {
					char tempTxt = txt.charAt(index);
					if (tempTxt != '-')
						temp = tempTxt;
				}
				array[i][j] = temp;
				index++;
			}
		}
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				res += array[i][j];
			}
		}
		return res.trim();
	}

	static String decryptByAnotheWay(int n, String txt) {
		String res = "";
		int row = txt.length() / n;
		row += (txt.length() % n) > 0 ? 1 : 0;
		for (int i = 0; i < row; i++) {
			for (int index = i; index < txt.length(); index += row) {
				char temTxt = txt.charAt(index);
				if (temTxt == '-')
					temTxt = ' ';
				res += temTxt;
			}
		}
		return res.trim();
	}
}
