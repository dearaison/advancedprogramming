package arrangenumber;

public class ArrangeNumber {

	public static void main(String[] args) {
		int[] a = { 89, 84, 85, 96, 95, 35, 48 };
		arrangeNumberInDescented(a);
	}

	static void arrangeNumberInDescented(int[] Numbers) {
		int sway;
		for (int i = 0; i < (Numbers.length - 1); i++) {
			for (int j = 0; j < (Numbers.length - i - 1); j++) {
				if (Numbers[j] < Numbers[j + 1]) {
					sway = Numbers[j];
					Numbers[j] = Numbers[j + 1];
					Numbers[j + 1] = sway;
				}
			}
		}
		for (int i1 = 0; i1 < Numbers.length; i1++) {
			System.out.print(Numbers[i1] + " ");
		}

	}

}
