package stransparentdemo;

import java.awt.*;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.*;

public class TranscluentWindow extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static void main(String[] args) throws IOException {
		JFrame.setDefaultLookAndFeelDecorated(true);
		new TranscluentWindow();
	}

	public TranscluentWindow() throws IOException {
		setUndecorated(true);
		setSize(400, 400);
		setBackground(new Color(0, 0, 0, 0));
		setContentPane(new TranslucentPane());
		// add(new JLabel(new
		// ImageIcon(ImageIO.read(getClass().getResource("T1.jpg")))));
		add(new JLabel(new ImageIcon(ImageIO.read(getClass().getResource("T5.gif")))));
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}
}
 
class TranslucentPane extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TranslucentPane() {
		setOpaque(false);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g.create();
		g2d.setComposite(AlphaComposite.SrcOver.derive(0.0f));
		g2d.setColor(getBackground());
		g2d.fillRect(0, 0, getWidth(), getHeight());
	}
}
