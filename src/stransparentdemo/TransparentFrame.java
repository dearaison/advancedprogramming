package stransparentdemo;

import java.awt.*;
import javax.swing.*;

public class TransparentFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TransparentFrame() {
		super("Transparent Windows");
		setBackground(new Color(0, 0, 0, 0));
		setSize(new Dimension(300, 200));
		JPanel panel = new JPanel() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void paintComponent(Graphics g) {
				Paint p = new GradientPaint(0.0f, 0.0f, new Color(255, 255, 255, 0), 0.0f, getHeight(),
						new Color(255, 255, 255, 0), false);
				((Graphics2D) g).setPaint(p);
				g.fillRect(0, 0, getWidth(), getHeight());
			}
		};
		setContentPane(panel);
		setLayout(new FlowLayout());
		getContentPane().add(new JButton("I am a Button"));

		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setVisible(true);
	}

	public static void main(String[] args) {
		JFrame.setDefaultLookAndFeelDecorated(true);
		new TransparentFrame();
	}
}
