package guessnumbergame;

import java.util.Random;
import java.util.Scanner;

public class GuessNumberGame {
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		GuessNumberGame demo = new GuessNumberGame();
		while (true) {
			demo.playGame();
			System.out.println("Do you want to play again (Y/N): ");
			String sc = new Scanner(System.in).nextLine();
			if (sc.equalsIgnoreCase("N")) {
				System.out.println("Bye, see you soon");
				break;
			}
		}
	}

	@SuppressWarnings("resource")
	public void playGame() {
		Random rd = new Random();
		int numberToGuess = rd.nextInt(101);
		System.out.println("The number which you need to guess is between 0 and 100");
		int count = 0;
		Scanner sc = new Scanner(System.in);
		while (true) {
			System.out.print("Enter the number which you think most exact: ");
			int number = sc.nextInt();
			count++;
			System.out.println("The number of guessing turn: " + count);
			if (number == numberToGuess) {
				System.out.println("You are winer, The number need guessing is: " + numberToGuess);
				break;
			} else {
				if (number > numberToGuess) {
					System.out.println("The number which you enter is higher than the guess number");
				} else {
					System.out.println("The number which you enter is lower than the guess number");

				}

			}
			if (count >= 7) {
				System.out.println("You are loser, The number need guessing is: " + numberToGuess);
				break;
			}
		}
	}
}
