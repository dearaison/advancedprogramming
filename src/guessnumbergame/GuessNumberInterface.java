package guessnumbergame;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import javax.swing.*;

@SuppressWarnings("serial")
public class GuessNumberInterface extends JFrame {
	JPanel panel, panel2;
	JTextField text;
	JButton button;
	JLabel announcement, numberOfTurn, enteringANumber;
	int count;
	Random rd = new Random();
	int numberToGuess = rd.nextInt(101);

	public GuessNumberInterface() {
		// TODO Auto-generated constructor stub
		count = 1;

		Container conten = getContentPane();
		setLayout(new BorderLayout());
		setTitle("Guess number Game");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);

		panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		numberOfTurn = new JLabel();
		numberOfTurn.setText("The number of guessing turn: " + count);
		panel.add(numberOfTurn);

		announcement = new JLabel("Announcement");
		announcement.setText("The number which you need to guess is between 0 and 100");
		panel.add(announcement);

		enteringANumber = new JLabel();
		enteringANumber.setText("Enter the number which you think most exact: ");
		panel.add(enteringANumber);

		text = new JTextField();
		panel.add(text);

		button = new JButton("Guess");
		button.setActionCommand("Guess");
		button.addActionListener(myListener);

		panel2 = new JPanel();
		panel2.setLayout(new FlowLayout());
		panel2.add(button);

		conten.add(panel, BorderLayout.NORTH);
		conten.add(panel2, BorderLayout.SOUTH);

		pack();
		setLocationRelativeTo(null);
	}

	ActionListener myListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			playGame();
		}
	};

	public void playGame() {
		count++;
		numberOfTurn.setText("The number of guessing turn: " + count);
		int number = Integer.parseInt(text.getText());
		if (number == numberToGuess) {
			announcement.setText("You are winer, The number need guessing is: " + numberToGuess);
		} else {
			if (number > numberToGuess) {
				announcement.setText("The number which you enter is higher than the guess number");
			} else {
				announcement.setText("The number which you enter is lower than the guess number");

			}

		}
		if (count >= 7) {
			announcement.setText("You are loser, The number need guessing is: " + numberToGuess);
		}
		text.setText("");
		pack();
		setLocationRelativeTo(null);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new GuessNumberInterface();
	}

}
