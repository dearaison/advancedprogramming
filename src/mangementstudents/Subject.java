package mangementstudents;

public class Subject {
	String name;
	float score;

	public Subject(String name, double score) {
		this.name = name;
		this.score = (float) score;
	}

	@Override
	public String toString() {
		return "Subject [name=" + name + ", score=" + score + "]";
	}

}
