package mangementstudents;

import java.util.ArrayList;

public class ManagementStudents {
	ArrayList<Student> listOfStudent;

	public ManagementStudents() {
		this.listOfStudent = new ArrayList<Student>();
	}

	public boolean addStudent(int iD, String name, String subjectName, double score) {
		Student st = searchStudentByID(iD);
		if (st == null) {
			Student newStudent = new Student(iD, name);
			newStudent.addSubject(subjectName, score);
			this.listOfStudent.add(newStudent);
			return true;
		} else {
			if (st.fullName.equals(name)) {
				st.addSubject(subjectName, score);
				return true;
			}

		}
		return false;

	}

	public Student searchStudentByID(int ID) {
		for (Student s : listOfStudent) {
			if (s.searchStudent(ID))
				return s;
		}
		return null;

	}

	public void printInformationOfStudent(Student st) {
		if (st != null){
		System.out.println("ID: " + st.ID);
		System.out.println("Name: " + st.fullName);
		System.out.println("Average score: " + st.toAverageGrade());}
		else {
			System.out.println("null");
		}
		
	}

	@Override
	public String toString() {
		return "ManagementStudents [listOfStudent=" + listOfStudent + "]";
	}

}
