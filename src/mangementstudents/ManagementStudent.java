package mangementstudents;

import junit.framework.TestCase;

public class ManagementStudent extends TestCase {
	// ("Math", 8.5)
	// ("Physics", 9.5)
	// ("Chemistry", 9.25)
	// ("Astronomy", 8.25)
	// ("Geography", 7.25)
	// ("History", 7.75)
	// ("Literary", 7.0)
	// ("Biology", 8.75)
	// ("English", 7.25)

	Student st1 = new Student(15130001, "Shiodome Miuna");
	Student st2 = new Student(15130002, "Toujou Kaede");
	Student st3 = new Student(15130210, "Joseph Maria");

	ManagementStudents list = new ManagementStudents();

	public void testManagementStudent() {
		// test addSubject method.
		st1.addSubject("Literary", 7.0);
		st1.addSubject("History", 7.75);
		st1.addSubject("Geography", 7.25);

		st2.addSubject("Chemistry", 9.25);
		st2.addSubject("Biology", 8.75);
		st2.addSubject("English", 7.25);

		st3.addSubject("Math", 8.5);
		st3.addSubject("Physics", 9.5);
		st3.addSubject("Chemistry", 9.25);

		// test addStudent method
		System.out.println(list.addStudent(15130210, "Joseph Maria", "Astronomy", 8.25));
		System.out.println(list.addStudent(15130003, "Kinomoto Sakura", "Mahou", 9.3));
		System.out.println(list.addStudent(15130002, "Fukukawa Sanae", "Psychology", 10.0));
		System.out.println(list.addStudent(15130002, "ABC", "Psychology", 10.0));
		System.out.println(list.addStudent(15130210, "Joseph Maria", "Math", 8.5));

		// test search method
		Student a = list.searchStudentByID(15130210);
		Student b = list.searchStudentByID(15130003);
		Student c = list.searchStudentByID(15132565);

		// test print informations student
		list.printInformationOfStudent(a);
		list.printInformationOfStudent(b);
		list.printInformationOfStudent(c);
		
		list.printInformationOfStudent(st1);
		list.printInformationOfStudent(st2);
		list.printInformationOfStudent(st3);

	}
}