package mangementstudents;

import java.util.ArrayList;

public class Student {
	int ID;
	String fullName;
	ArrayList<Subject> listOfSubject;

	public Student(int iD, String fullName) {
		ID = iD;
		this.fullName = fullName;
		this.listOfSubject = new ArrayList<Subject>();
	}

	public void addSubject(String name, double score) {
		this.listOfSubject.add(new Subject(name, score));
	}

	public float toAverageGrade() {
		float totalScore = 0;
		for (Subject s : listOfSubject) {
			totalScore += s.score;
		}
		float averageScore = totalScore / listOfSubject.size();
		return averageScore;

	}

	public boolean searchStudent(int iD) {
		return this.ID == iD;
	}

	@Override
	public String toString() {
		return "Student [ID=" + ID + ", fullName=" + fullName + ", listOfSubject=" + listOfSubject + "]";
	}

}
